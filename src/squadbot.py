import discord, io, os, sys, config, sqlite3, re, shutil, filecmp, requests, pymysql
from discord.ext import tasks
from contextlib import closing
from datetime import datetime
import pymysql.cursors
from dateutil import parser

DEBUG = False
regex_GroupOfIDs = "^([0-9]{17}[ |,]+){0,4}[0-9]{17}[ |,]*$"
regex_SingleID = "^[0-9]{17}$"
regex_MessageID = "^[0-9]{18}$"
standardFooter = "Made with ♥ by Sentennial#1234"

#####################################
######### CLASS SquadClient #########
class SquadClient(discord.Client):
    isReady = False
    updateMessageObject = None
    layersWithTeams = list()

    async def on_ready(self):
        print('Logged on as {0}!'.format(self.user))
        if (DEBUG): await self.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name='DEBUG MODE'))
        else: await self.change_presence(activity=discord.Activity(type=discord.ActivityType.playing, name='Squad 24/7'))
        self.isReady = True
    
    ###### Discord Events ######

    async def on_message(self, message):
        if cfg['featureEnable_BanReviewQueue'] and (message.author != client.user and message.channel.id == cfg['ban_evidence_channel_ID']):
            await self.onmessageRecordBans(message)
        
        if (cfg['featureEnable_WhitelistManagement'] and message.author != client.user and message.channel.id == cfg['steamID_gather_channel_ID']):
            await self.onmessageGatherSteamIDs(message)
        
        if (cfg['featureEnable_RotationManagement'] and message.author != client.user and message.channel.id == cfg['rotation_upload_channel_ID']):
            await self.onmessageUploadRotation(message)

        if (cfg['featureEnable_RotationManagement'] and message.author != client.user and message.content.lower().startswith('!rotation')):
            await self.onmessageRotationCommand(message)

        if (cfg['featureEnable_BanReviewQueue'] and message.author != client.user and message.content.lower().startswith('!banqueue') and message.guild.get_role(cfg['discord_executiveadmin_ID']) in message.author.roles):
            await self.onmessageBanQueueCommand(message)

        if (cfg['featureEnable_BanReviewQueue'] and message.author != client.user and message.content.lower().startswith('!banremove') and message.guild.get_role(cfg['discord_executiveadmin_ID']) in message.author.roles):
            await self.onmessageBanRemoveCommand(message)
        
        if (cfg['featureEnable_StatTracker'] and message.author != client.user and message.content.lower().startswith('!stats')):
            await self.onmessageStats(message)

        if (cfg['featureEnable_StatTracker'] and message.author != client.user and message.content.lower().startswith('!link')):
            await self.onmessageLink(message)

        if (cfg['featureEnable_MiscUtils'] and message.author != client.user and message.content.lower().startswith('!convert')):
            await self.onmessageDate(message)


    async def on_message_delete(self, message):
        if (cfg['featureEnable_BanReviewQueue'] and message.author != client.user and message.channel.id == cfg['ban_evidence_channel_ID']):
            await self.onmessageDeleteBanFromDB(message)

    async def on_reaction_add(self, reaction, user):
        if (cfg['featureEnable_BanReviewQueue'] and user != client.user and reaction.message.channel.id == cfg['ban_evidence_channel_ID']):
            await self.onmessageReactionAdd(reaction, user)

    ###### Methods ######

    async def onmessageDate(self, message):
        try:
            epoch = str(parser.parse(message.content.replace('!convert ', '')).timestamp()).replace('.0','')
        except:
            await message.reply('Error, I cannot convert `'+message.content.replace('!convert ', '')+'`. Try to reformat it.')
            return
        await message.reply('<t:'+epoch+'> is <t:'+epoch+':R>. `<t:'+epoch+'> & <t:'+epoch+':R>`')

    async def onmessageStats(self, message):
        print('STATS: Handling message: ' + message.content)
        messageWithoutCmd = message.content.replace('!stats', '').strip()
        userSteamID = None
        selfLookup = False
        # Use the steamID of the mentioned user
        if (len(message.mentions) > 0):
            discordID = message.mentions[0].id
        elif (len(messageWithoutCmd) == 0):
            discordID = message.author.id
            selfLookup = True
        else:
            userSteamID = messageWithoutCmd
        if (userSteamID is None):
            # Get SteamID
            try:
                with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
                    with closing(sqlite.cursor()) as sqlitecursor:
                        rows = sqlitecursor.execute("SELECT steamID FROM statsSteamIDs WHERE discordID = ?", (discordID,)).fetchone()
                        userSteamID = rows[0]
            except:
                await message.reply('SteamID was not found, please use `!link YourSteamIDhere`. If you @mentioned another user, they need to use `!link`. You can also do `!stats anySteamIDhere` to look up based on steamID')
                return
        print("Looking up steamID: " + userSteamID)
        
        # Get Stats from DB
        conn = pymysql.connect(host=cfg['mySQL_Host'], user=cfg['mySQL_User'], password=cfg['mySQL_Password'], database='squadjs', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
        with conn:
            with conn.cursor() as curs:
                ## Player Name
                statPlayerName = 'none'
                sql = "SELECT * FROM squadjs.DBLog_SteamUsers WHERE `steamID` = %s LIMIT 1;"
                curs.execute(sql, (userSteamID, ))
                res = curs.fetchone()
                statPlayerName = res['lastName']

                ## Kills
                statKills = 'none'
                sql = "SELECT COUNT(*) as kills FROM squadjs.DBLog_Deaths WHERE `attacker` = %s AND `attackerTeamID` != `victimTeamID`;"
                curs.execute(sql, (userSteamID, ))
                res = curs.fetchone()
                statKills = res['kills']

                ## Deaths
                statDeaths = 'none'
                sql = "SELECT COUNT(*) as deaths FROM squadjs.DBLog_Deaths WHERE `victim` = %s AND `attackerTeamID` != `victimTeamID`;"
                curs.execute(sql, (userSteamID, ))
                res = curs.fetchone()
                statDeaths = res['deaths']

                ## Incaps
                statIncaps = 'none'
                sql = "SELECT COUNT(*) as incaps FROM squadjs.DBLog_Wounds WHERE `attacker` = %s AND `attackerTeamID` != `victimTeamID`;"
                curs.execute(sql, (userSteamID, ))
                res = curs.fetchone()
                statIncaps = res['incaps']

                ## Matches
                statMathes = 'none'
                sql = "SELECT COUNT(*) as matches FROM ( SELECT `victim` FROM squadjs.DBLog_Wounds WHERE `victim` = %s GROUP BY `match` ) as `match`;"
                curs.execute(sql, (userSteamID, ))
                res = curs.fetchone()
                statMathes = res['matches']

                ## K/D
                statKD = round(statKills/statDeaths, 2)
                ## Create message
                statEmbed = discord.Embed(
                    title=statPlayerName, 
                    color=discord.Colour.gold(),
                    description="Squad Stats on the CD Server")
                statEmbed.set_footer(text="Work in progress - " + standardFooter)
                if (selfLookup):
                    statEmbed.set_thumbnail(url=message.author.avatar_url_as(size=512))
                else:
                    statEmbed.set_thumbnail(url=cfg['CommunityLogoUrlPNG'])

                statEmbed.add_field(name="Offensive", inline=True, 
                value="" + 
                "\n__Kills:__ " + str(statKills) + 
                "\n__Deaths:__" + str(statDeaths) + 
                "\n__K/D:__" + str(statKD) + 
                "\n__Incaps:__" + str(statIncaps)
                )
                statEmbed.add_field(name="Other", inline=True, 
                value="" + 
                "\n__Matches:__ " + str(statMathes)
                )

                await message.reply(embed=statEmbed)

    async def onmessageLink(self, message):
        userEntry = message.content.replace('!link ', '')
        if (re.match(regex_SingleID, userEntry)): 
            with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
                with closing(sqlite.cursor()) as sqlitecursor:
                    rows = sqlitecursor.execute("SELECT steamID FROM statsSteamIDs WHERE discordID = ?", (message.author.id,)).fetchall()
                    if (len(rows) == 1):
                        sqlitecursor.execute("UPDATE statsSteamIDs SET steamID = ? WHERE discordID = ?", (userEntry, str(message.author.id)))
                    else:
                        sqlitecursor.execute("INSERT INTO statsSteamIDs(discordID,steamID) VALUES (?,?)", (str(message.author.id), userEntry))
                    await message.reply('Success. `'+userEntry+'` linked. Now try `!stats`')
                    sqlite.commit()
                
        else:
            await message.reply('Error. `'+userEntry+'` does not appear to be a steamID, please double check')
        pass

    async def onmessageBanRemoveCommand(self, message):
        messageID = message.content.replace('!banremove ', '').strip()
        if (re.match(regex_MessageID, messageID)): 
            await message.reply("Removing ID " + messageID + " from review queue")
            with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
                with closing(sqlite.cursor()) as sqlitecursor:
                    sqlitecursor.execute("DELETE FROM banhistory WHERE messageID = ?", (messageID, ))
                sqlite.commit()
            await self.updateStatusMessage()
        else:
            await message.reply("That doesn't seem to be a valid ID.")

    async def onmessageReactionAdd(self, reaction, user):
        status=""
        if (str(reaction.emoji) == cfg['banEmoji_I_Upped']):
            print("Marking ban as upped: " + reaction.message.content)
            await reaction.message.clear_reactions()
            await reaction.message.add_reaction(cfg['banEmoji_Was_Upped'])
            await reaction.message.add_reaction(cfg['banEmoji_Recheck'])
            status = "upped"

        if (str(reaction.emoji) == cfg['banEmoji_No_Up_Needed']):
            print("Marking ban as not upped: " + reaction.message.content)
            await reaction.message.clear_reactions()
            await reaction.message.add_reaction(cfg['banEmoji_Was_Not_Upped'])
            await reaction.message.add_reaction(cfg['banEmoji_Recheck'])
            status = "notupped"
        if (str(reaction.emoji) == cfg['banEmoji_Recheck']):
            print("Re-recording ban: " + reaction.message.content)
            await reaction.message.clear_reactions()
            await reaction.message.add_reaction(cfg['banEmoji_I_Upped'])
            await reaction.message.add_reaction(cfg['banEmoji_No_Up_Needed'])
            status = "pending"
        if (status != ""):
            #update db
            with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
                with closing(sqlite.cursor()) as sqlitecursor:
                    sqlitecursor.execute("UPDATE banhistory SET status = ? WHERE messageID = ?", (status, str(reaction.message.id)))
                sqlite.commit()
            await self.updateStatusMessage()

    async def onmessageDeleteBanFromDB(self, message):
        # Remove a ban from DB if its message is deleted
        if (len( message.content) == 0):
            return
        print("Deleting ban from DB: " + message.content)
        with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
            with closing(sqlite.cursor()) as sqlitecursor:
                sqlitecursor.execute("DELETE FROM banhistory WHERE messageID = ?", (str(message.id), ))
            sqlite.commit()
        await self.updateStatusMessage()

    async def updateStatusMessage(self):
        statusEmbed = discord.Embed(title="New Bans Needing Review", color=discord.Colour.red())
        statusEmbed.set_footer(text=standardFooter + '. Updated ' + datetime.today().strftime('%Y-%m-%d'))
        
        with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
            with closing(sqlite.cursor()) as sqlitecursor:
                pendingBansRows = sqlitecursor.execute("SELECT * FROM banhistory WHERE status = 'pending'").fetchall()
        for pendingBan in pendingBansRows:
            statusEmbed.add_field(name=pendingBan[4], value="Admin: " + pendingBan[2] + "  •  Date: " + str(pendingBan[3]) + "  •  [link to ban](" + pendingBan[1] + ").", inline=False)
        
        if len(pendingBansRows) == 0:
            statusEmbed.add_field(name="Empty", value="-- Ban review queue is currently empty, good job! --", inline=False)

        await self.get_channel(cfg['ban_status_channel_ID']).purge(limit=5, check=self.is_me)
        self.updateMessageObject = await self.get_channel(cfg['ban_status_channel_ID']).send(embed=statusEmbed)

    async def onmessageBanQueueCommand(self, message):
        await self.updateStatusMessage()
        await message.reply("Status updated, see " + self.updateMessageObject.jump_url)
    
    async def onmessageRecordBans(self, message):
        if (len( message.content) == 0):
            return

        print("Recording ban: " + message.content)
        await message.add_reaction(cfg['banEmoji_I_Upped'])
        await message.add_reaction(cfg['banEmoji_No_Up_Needed'])

        # Add ban to DB
        with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
            with closing(sqlite.cursor()) as sqlitecursor:
                sqlitecursor.execute("INSERT INTO banhistory(messageID, jumpURL, discordName, createdAt, messageContents, status) VALUES (?,?,?,?,?,?)", 
                (str(message.id), message.jump_url, message.author.name, datetime.today().strftime('%Y-%m-%d'), message.content, "pending") )
            sqlite.commit()
        await self.updateStatusMessage()

    async def onmessageRotationCommand(self, message):
        await self.sendRotationEmbed(message.channel.id)  
    
    async def sendRotationEmbed(self, channelID):
        CDDiscordServer = client.get_guild(cfg['DiscordServer_ID'])
        publicRotationCh = CDDiscordServer.get_channel(channelID)
        
        layerDict = dict(self.layersWithTeams)
        layerEmbed = discord.Embed(title=cfg['CommunityName']+" Layer Rotation", color=discord.Colour.gold())
        layerEmbed.set_thumbnail(url=cfg['CommunityLogoUrlPNG'])
        layerEmbed.set_footer(text=standardFooter + '. Updated ' + datetime.today().strftime('%Y-%m-%d'))
        with open(cfg['path_to_ServerConfig_dir'] + 'LayerRotation.cfg', "r") as layers:
            for line in layers:
                if (line.startswith('/') or len(line) < 2):
                    continue
                body = layerDict[line.strip('\n')].strip() + ' [Click for map](https://squadmaps.com/img/maps/full_size/'+line.strip('\n')+'.jpg)'
                body = body.strip()
                layerEmbed.add_field(name=line.strip('\n'), value=body, inline=False)

        await publicRotationCh.send(embed=layerEmbed)

    async def onmessageUploadRotation(self, message):
        CDDiscordServer = client.get_guild(cfg['DiscordServer_ID'])
        publicRotationCh = CDDiscordServer.get_channel(cfg['discord_public_rotation_ID'])

        if (len(message.attachments) == 0): #Message doesn't have an attachment
            print('Deleting ' + message.content)
            await message.delete()
        else: #Message has attachment
            if ('text' in message.attachments[0].content_type ):
                print(message.attachments[0].content_type)
                await message.attachments[0].save(cfg['path_to_ServerConfig_dir'] + 'LayerRotation.cfg')
                await message.channel.send(message.attachments[0].filename + ' uploaded to server, post created in ' + publicRotationCh.mention)
                await publicRotationCh.purge(limit=5, check=self.is_me)
                await self.sendRotationEmbed(cfg['discord_public_rotation_ID'])

    async def onmessageGatherSteamIDs(self, message):
        # Does message contain between 1 and 5 steamIDs separated by either commas or spaces
        if (re.match(regex_GroupOfIDs, message.content)): 
            print("handling group submission from "+message.author.name+": " + message.content)
            # clean up input
            idsFromMsg = re.sub(' +', ' ', message.content.strip("\n").replace(",", " ")).split(' ')
            firstID = ''
            friendIDs = []
            numIDsRecorded = 0
            # Collect IDs into variables
            for id in idsFromMsg:
                if (numIDsRecorded == 5): break
                if (numIDsRecorded == 0):
                    firstID = id
                else:
                    friendIDs.append(id)    
                print(id)
                numIDsRecorded += 1
            
            # Add to DB
            with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
                with closing(sqlite.cursor()) as sqlitecursor:
                    today = datetime.today().strftime('%Y-%m-%d')
                    # Update or insert primary user's steamID
                    rows = sqlitecursor.execute("SELECT steamID, changedOn FROM steamIDs WHERE discordID = ?", (message.author.id,)).fetchall()
                    # user already has a steamID recorded
                    if (len(rows) == 1):
                        idInDB = rows[0][0]
                        changedOn = rows[0][1]
                        # Rate Limit, if debug=false, and user is not me
                        if (today == changedOn and DEBUG == False and message.author.id != 177189581060308992): 
                            errorEmbed = discord.Embed(title="ERROR", color=discord.Colour.red(), description=""+
                            "In order to prevent abuse, you can only submit here once per day.")
                            errorEmbed.set_footer(text=standardFooter)
                            await message.channel.send(f"{message.author.mention}", embed=errorEmbed)
                            await message.delete()
                            return
                        if (idInDB == firstID):
                            pass # no change
                        else: #update steamID in DB
                            sqlitecursor.execute("UPDATE steamIDs SET steamID = ?, discordName = ?, changedOn = ? WHERE discordID = ?", (firstID, message.author.name, today, str(message.author.id)))
                    else: # add new 
                        sqlitecursor.execute("INSERT INTO steamIDs(discordID,steamID,discordName,changedOn) VALUES (?,?,?,?)", (str(message.author.id), firstID, message.author.name, today))
                    
                    # Remove all IDs associated with primary user, then add entered friend IDs to DB
                    sqlitecursor.execute("DELETE FROM linkedSteamIDs WHERE discordID = ?", (str(message.author.id),))
                    for friendID in friendIDs:
                        sqlitecursor.execute("INSERT INTO linkedSteamIDs(discordID,steamID) VALUES (?,?)", (str(message.author.id), friendID))
                sqlite.commit()

            if (numIDsRecorded == 1):
                embed = discord.Embed(title="Thank you!", color=discord.Colour.green(), description=""+
                "Your ID `"+firstID+"` has been recorded")
                embed.set_footer(text=standardFooter)
                await message.channel.send(f"{message.author.mention}", embed=embed)
            elif (numIDsRecorded == 2):
                embed = discord.Embed(title="Thank you!", color=discord.Colour.green(), description=""+
                "Your ID `"+firstID+"` and your friend \n`"+str(friendIDs)+"` \nhave been recorded")
                embed.set_footer(text=standardFooter)
                await message.channel.send(f"{message.author.mention}", embed=embed)
            else:
                embed = discord.Embed(title="Thank you!", color=discord.Colour.green(), description=""+
                "Your ID `"+firstID+"` and your friends \n`"+str(friendIDs)+"` \nhave been recorded")
                embed.set_footer(text=standardFooter)
                await message.channel.send(f"{message.author.mention}", embed=embed)

            await message.delete()
            return

        # IF WE GET HERE, MSG NOT VALID
        errorEmbed = discord.Embed(title="ERROR", color=discord.Colour.red(), 
        description="`"+message.content+"` doesn't appear to be valid, please try again."+
        "\nIf you are submitting steamIDs for yourself and up to 4 friends, please follow the format of: "+
        "\n`yourid, friend1id, friend2id, friend3id, friend4id`"+
        "\nON A SINGLE LINE."+ 
        "\nYou can submit less than 4 friends if you wish."+
        "\nYou can find your steamID64 [by clicking here](https://steamid.io/)")
        errorEmbed.set_footer(text="Made with ♥ by Sentennial")

        await message.channel.send(f"{message.author.mention}", embed=errorEmbed)

    def loadLayerInfo(self):
        with open('layersWithTeams.txt', "r") as layersAndTeams:
            for line in layersAndTeams:
                s = line.split(sep=',')
                self.layersWithTeams.append((s[0], s[1]))

    def is_me(self, m):
        return m.author == client.user
######### END CLASS SquadClient #########
#########################################


intents = discord.Intents.default()
intents.members = True
client = SquadClient(intents=intents)

# Get SteamIDs+Names of discord users with single Patreon role, as well as any SteamIDs of their friends if they have the group Patreon role.
async def getValidIDsAndNames():
    CDDiscordServer = client.get_guild(cfg['DiscordServer_ID'])
    whitelistRoleP1 = CDDiscordServer.get_role(cfg['discord_whitelist_patreon_single_ID']) # Single whitelist
    whitelistRoleP5 = CDDiscordServer.get_role(cfg['discord_whitelist_patreon_five_ID']) # Five whitelists
    whitelistRole = CDDiscordServer.get_role(cfg['discord_whitelist_manual_ID']) # Manually assigned Whitelist role

    with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
        with closing(sqlite.cursor()) as sqlitecursor:
            rows = sqlitecursor.execute("SELECT steamID, discordID, discordName, changedOn FROM steamIDs").fetchall()
            steamIDs = []
            for row in rows: # Loop through SteamID, discordID from database
                steamID = row[0]
                discordID = row[1]
                discordName = row[2]
                changedOn = row[3]
                # If user is no longer a member of the server, pass
                if (CDDiscordServer.get_member(int(discordID)) is None):
                    print("Discord user " + discordName + " (" + discordID + ") no longer a member of the server, removing from database.")
                    sqlitecursor.execute("DELETE FROM steamIDs WHERE steamID = ?", (steamID, ))
                    sqlite.commit()
                    continue
                try:
                    membersRoles = CDDiscordServer.get_member(int(discordID)).roles
                except:
                    print("Error in getValidIDsAndNames: Couldn't get roles of discord member: " + discordID)
                    continue
                # If user has group patreon role
                if (whitelistRoleP5 in membersRoles):
                    friendsIDs = sqlitecursor.execute("SELECT steamID FROM linkedSteamIDs WHERE discordID = ?", (discordID,)).fetchall()
                    # add main user's ID
                    steamIDs.append( (steamID, str('DiscordName: ' + discordName + ' Group Whitelist Tier, Friends: '+str(len(friendsIDs)) + '. Updated on ' + changedOn )) )
                    # add friends IDs
                    for friendIDTuple in friendsIDs:
                        friendID = friendIDTuple[0]
                        steamIDs.append( (friendID, str('Friend linked to ' + steamID)) )

                # user has single patreon role
                elif (whitelistRole in membersRoles or whitelistRoleP1 in membersRoles): # They are allowed a single steamID
                    steamIDs.append( (steamID, str('DiscordName: ' + discordName + ' Single Whitelist Tier. Updated on ' + changedOn)) )
    return steamIDs

if (DEBUG): updateFreq = 3
else: updateFreq = 30
@tasks.loop(seconds = updateFreq)
async def checkWhiteList():
    if (client.isReady == False):
        return

    validIDsAndNames = await getValidIDsAndNames()

    CurrentConfig = 'Admins.cfg.current'
    tmpConfig = 'Admins.cfg.tmp'
    AdminsCfgFileAndPath = cfg['path_to_ServerConfig_dir']+'Admins.cfg'
    ownerAndGroupPerms = os.stat(AdminsCfgFileAndPath)

    shutil.copy2(AdminsCfgFileAndPath, CurrentConfig)

    # Delete all whitelists
    with open(CurrentConfig, "r") as input:
        with open(tmpConfig, "w") as output:
            # iterate all lines from file
            for line in input:
                # copy over everything except whitelists the bot added
                if (not re.match("^Admin=[0-9]{17}:WhitelistBot.*", line.strip("\n"))):
                    output.write(line)
            # add back valid IDs for whitelist
            for validUser in validIDsAndNames:
                output.write('Admin='+validUser[0]+':WhitelistBot // '+validUser[1]+'\n')
    # replace perms
    #os.chown(tmpConfig, ownerAndGroupPerms.st_uid, ownerAndGroupPerms.st_gid)

    # Check if files are identical byte-by-byte
    if (filecmp.cmp(tmpConfig, CurrentConfig, shallow=False) == True):
        # if(DEBUG): print("CFGs are same, skipping")
        pass
    else:
        # Copy tmp config to live config
        print("CFG changed, copying")
        
        with open(CurrentConfig, "r") as input:
            with open(AdminsCfgFileAndPath, "w") as output:
            # iterate all lines from file
                for line in input:
                    # copy over everything except whitelists the bot added
                    if (not re.match("^Admin=[0-9]{17}:WhitelistBot.*", line.strip("\n"))):
                        output.write(line)
                # add back valid IDs for whitelist
                for validUser in validIDsAndNames:
                    output.write('Admin='+validUser[0]+':WhitelistBot // '+validUser[1]+'\n')
        
        # shutil.copy2(tmpConfig, AdminsCfgFileAndPath)
    if(os.path.exists(tmpConfig)):
        os.remove(tmpConfig)
    if(os.path.exists(CurrentConfig)):
        os.remove(CurrentConfig)
    return

if __name__ == "__main__":
    if(os.path.exists('configDEBUG.cfg')):
        print('Loading DEBUG config')
        cfg = config.Config('configDEBUG.cfg')
        DEBUG = True
    elif(os.path.exists('config.cfg')):
        print('Loading production config')
        cfg = config.Config('config.cfg')
        DEBUG = False
    else:
        print("ERROR - no config file found. Aborted")
        exit()
    
    with closing(sqlite3.connect(cfg['path_to_SteamID_DB'])) as sqlite:
        with closing(sqlite.cursor()) as sqlitecursor:
            # Create database if it doesn't exist
            sqlitecursor.execute("CREATE TABLE IF NOT EXISTS steamIDs (discordID TEXT NOT NULL PRIMARY KEY, steamID TEXT NOT NULL, discordName TEXT DEFAULT ' ', changedOn TEXT NOT NULL )")
            sqlitecursor.execute("CREATE TABLE IF NOT EXISTS linkedSteamIDs (discordID TEXT NOT NULL, steamID TEXT NOT NULL)")
            sqlitecursor.execute("CREATE TABLE IF NOT EXISTS banhistory (messageID TEXT NOT NULL PRIMARY KEY, jumpURL TEXT NOT NULL, discordName TEXT NOT NULL, createdAt TEXT NOT NULL, messageContents TEXT NOT NULL, status TEXT NOT NULL )")
            sqlitecursor.execute("CREATE TABLE IF NOT EXISTS statsSteamIDs (discordID TEXT NOT NULL PRIMARY KEY, steamID TEXT NOT NULL)")
            sqlite.commit()

    print('started')
    checkWhiteList.start()
    client.loadLayerInfo()
    client.run(cfg['discord_token'])
